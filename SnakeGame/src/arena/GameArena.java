package arena;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import models.Action;
import models.Action.CATEGORY;
import models.Cell;
import models.CellNode;
import models.Direction;
import models.Food;
import models.MazeInfo;
import models.Node;
import models.Snake;
import simulation.GridGame;
public class GameArena extends GridGame{

	private ArrayList<Snake> snakes;
	private CellNode[][] arenaStatus;
	private Random random;
	private Food food;
	public GameArena(int gridWidth, int gridHeight, int gridSquareSize, int frameRate) {

		super(gridWidth, gridHeight, gridSquareSize, frameRate);
		this.random = new Random();
		this.snakes = new ArrayList<Snake>();
		this.arenaStatus = new CellNode[getGridWidth()][getGridHeight()];
		for(int x=0;x<getGridWidth();x++) 
			for(int y=0;y<getGridHeight();y++) 
				this.arenaStatus[x][y] = new CellNode(new Point(x,y));		
		
		for(int x=0;x<getGridWidth();x++) {
			for(int y=0;y<getGridHeight();y++) {
				CellNode node = this.arenaStatus[x][y];
				if(isPointInGrid(new Point(x,y-1)))
					node.setUP(this.arenaStatus[x][y-1]);
				if(isPointInGrid(new Point(x,y+1)))
					node.setDOWN(this.arenaStatus[x][y+1]);
				if(isPointInGrid(new Point(x-1,y)))
					node.setLEFT(this.arenaStatus[x-1][y]);
				if(isPointInGrid(new Point(x+1,y)))
					node.setRIGHT(this.arenaStatus[x+1][y]);
			}
		}
		if(Snake.algo == 3) {
        	int x1 = getGridWidth()/2 - 2;
        	int x2 = getGridWidth()/2 + 2;
        	int y1 = getGridHeight()/2 - 2 ;
        	int y2 = getGridHeight()/2 + 2 ;
        	for(int x=x1;x<=x2;x++)
        		for(int y=y1;y<=y2;y++) {
        			this.arenaStatus[x][y].setColor(Color.BLACK);
        			this.arenaStatus[x][y].setCell(Cell.FULL);
        		}
        		
        	
        	x1 = getGridWidth()/2 - 10;
        	x2 = getGridWidth()/2 - 6 ;
        	y1 = getGridHeight()/2 - 5 ;
        	y2 = getGridHeight()/2 - 1 ;
        	for(int x=x1;x<=x2;x++)
        		for(int y=y1;y<=y2;y++) {
        			this.arenaStatus[x][y].setColor(Color.GREEN);
        			this.arenaStatus[x][y].setCell(Cell.OBSTACLE);	
        		}
        			
        	
        	x1 = getGridWidth()/2 + 6;
        	x2 = getGridWidth()/2 + 10 ;
        	y1 = getGridHeight()/2 - 8 ;
        	y2 = getGridHeight()/2 - 4;
        	for(int x=x1;x<=x2;x++)
        		for(int y=y1;y<=y2;y++) { 
        			this.arenaStatus[x][y].setColor(Color.PINK);
        			this.arenaStatus[x][y].setCell(Cell.OBSTACLE);
        		}
        }
		this.createSnake(new Snake());
		this.food = createFood(this.snakes.get(0));
		this.addDrawable(food);

	}

	private void createSnake(Snake snake) {
		for (Node node: snake.getSnakeBody()) {
			this.arenaStatus[node.getXCoord()][node.getYCoord()].setCell(Cell.FULL);
		}
		this.snakes.add(snake);
		this.addDrawable(snake);
	}

	@Override
	protected void timerTick() {
		ArrayList<Snake> snakes = new ArrayList<Snake>(this.snakes);
		
		for (Snake snake : snakes) {

			Node head = snake.getSnakeBody().get(0);
			Action action = snake.chooseAction(this.createMazeInfo(snake));
			this.uptadeArena(snake, Cell.EMPTY);

			if (action != null) {
				if (action.getCat() == CATEGORY.REPRODUCE) {
					Snake newSnake = snake.reproduce();
					this.createSnake(newSnake);
				} else if (action.getCat() == CATEGORY.EAT && snake.canEatFood(this.food)) {
					snake.eatFood(this.food);
					
					for (Snake s : snakes) {
						if(Snake.algo == 1)
							s.clearBFS();
						else if(Snake.algo == 2)
							s.clearDFS();
						else if(Snake.algo == 3)
							s.clearAStar();
					}					
					this.arenaStatus[this.food.getXCoord()][this.food.getYCoord()].setCell(Cell.EMPTY);
					this.removeDrawable(this.food);
					this.food = this.createFood(snake);
					this.addDrawable(this.food);
				} else if (action.getCat() == CATEGORY.MOVE) {
					if (this.isDirectionFree(head.getLocation(),action.getDir())) {
						snake.move(action.getDir());
					}
				}
			}
			this.uptadeArena(snake, Cell.FULL);
		}
	}
	
	private boolean isPointFree(Point point) {

		return isPointInGrid(point) && this.arenaStatus[point.x][point.y].getCell() != Cell.FULL;
	}

	private boolean isPointInGrid(Point point) {

		return (point.x >= 0 && point.x < getGridWidth()) && (point.y >= 0 && point.y < getGridHeight());
	}
	
	private boolean isDirectionFree(Point point, Direction direction) {

		if (direction == null) {
			return false;
		}

		int x = point.x;
		int y = point.y;

		if (direction == Direction.UP) {
			y--;
		} else if (direction == Direction.DOWN) {
			y++;
		} else if (direction == Direction.LEFT) {
			x--;
		} else if (direction == Direction.RIGHT) {
			x++;
		}

		return isPointFree(new Point(x, y));
	}
	
	private Food createFood(Snake snake) {
		CellNode[][] currentStatus = this.arenaStatus;
		
		for (Node node : snake.getSnakeBody()) {
			currentStatus[node.getXCoord()][node.getYCoord()].setCell(Cell.FULL);
		}
		
		int x = random.nextInt(this.getGridWidth());
		int y = random.nextInt(this.getGridHeight());
		while(currentStatus[x][y].getCell() != Cell.EMPTY) {
			x = random.nextInt(this.getGridWidth());
			y = random.nextInt(this.getGridHeight());
		}
		this.arenaStatus[x][y].setCell(Cell.FOOD);
		return new Food(new Point(x, y));
	}
	
	private MazeInfo createMazeInfo(Snake snake) {

		Node head = snake.getSnakeBody().get(0);

		ArrayList<Direction> freeDirections = new ArrayList<>();
		if (this.isDirectionFree(head.getLocation(), Direction.DOWN)) {
			freeDirections.add(Direction.DOWN);
		} else if (this.isDirectionFree(head.getLocation(), Direction.UP)) {
			freeDirections.add(Direction.UP);
		} else if (this.isDirectionFree(head.getLocation(), Direction.LEFT)) {
			freeDirections.add(Direction.LEFT);
		} else if (this.isDirectionFree(head.getLocation(), Direction.RIGHT)) {
			freeDirections.add(Direction.RIGHT);
		}
		
		HashMap<Direction, Cell> cellValues = new HashMap<Direction,Cell>();
		cellValues.put(Direction.DOWN, this.getCellValue(new Point(head.getXCoord(),head.getYCoord()+1)));
		cellValues.put(Direction.UP, this.getCellValue(new Point(head.getXCoord(),head.getYCoord()-1)));
		cellValues.put(Direction.LEFT, this.getCellValue(new Point(head.getXCoord()-1,head.getYCoord())));
		cellValues.put(Direction.RIGHT, this.getCellValue(new Point(head.getXCoord()+1,head.getYCoord())));
		
		return new MazeInfo(this.getGridWidth(), this.getGridHeight(), this.food, 
									this.arenaStatus, cellValues, freeDirections);
	}
	
	private void uptadeArena(Snake snake, Cell status) {
		for (Node node: snake.getSnakeBody()) {
			this.arenaStatus[node.getXCoord()][node.getYCoord()].setCell(status);
		}
	}
	private Cell getCellValue(Point point) {
		if (isPointInGrid(point)) {
			return this.arenaStatus[point.x][point.y].getCell();
		} else {
			return Cell.FULL;
		}
	}
}
