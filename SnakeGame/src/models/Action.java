package models;

public class Action {
	
	public enum CATEGORY{
		REPRODUCE,
		EAT,
		MOVE
	}
	
	private final CATEGORY cat;
	private final Direction dir;

	public Action(CATEGORY cat) {
		this.cat = cat;
        this.dir = null;
	}
	
	public Action(CATEGORY cat, Direction dir) {
		this.cat = cat;
        this.dir = dir;
	}

	public CATEGORY getCat() {
		return cat;
	}

	public Direction getDir() {
		return dir;
	}


}
