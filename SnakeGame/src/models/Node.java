package models;

import java.awt.Point;

public class Node extends Point{
	private Point location;
	
	public Node(Point currentPoint) {
		this.location = new Point(currentPoint.x,currentPoint.y);	
	}
	
	public Point getLocation() {
		return this.location;
	}
	
	public int getXCoord() {
		return  this.location.x;
	}

	public int getYCoord() {
		return  this.location.y;
	}
	
	public void setXCoord(int x) {
		this.location.x = x;
	}

	public void setYCoord(int y) {
		this.location.y = y;
	}
}
