package models;

import java.util.ArrayList;
import java.util.HashMap;

public class MazeInfo {

	private int gridWidth;

	private int gridHeight;

	private Food food;
	
	private CellNode[][] arenaStatus;
	
	private HashMap<Direction, Cell> cellValues;
	
	private ArrayList<Direction> freeDirections;

	public MazeInfo(int gridWidth, int gridHeight, Food food, CellNode[][] arenaStatus, HashMap<Direction,
			Cell> cellValues, ArrayList<Direction> freeDirections) {
		this.gridWidth = gridWidth;
		this.gridHeight = gridHeight;
		this.food = food;
		this.arenaStatus = arenaStatus;
		this.cellValues = cellValues;
		this.freeDirections = freeDirections;
	}

	public Food getFood() {
		return this.food;
	}

	public int getGridWidth() {
		return gridWidth;
	}

	public void setGridWidth(int gridWidth) {
		this.gridWidth = gridWidth;
	}

	public int getGridHeight() {
		return gridHeight;
	}

	public void setGridHeight(int gridHeight) {
		this.gridHeight = gridHeight;
	}

	public ArrayList<Direction> getFreeDirections() {
		return freeDirections;
	}

	public void setFreeDirections(ArrayList<Direction> freeDirections) {
		this.freeDirections = freeDirections;
	}

	public CellNode[][] getArenaStatus() {
		return arenaStatus;
	}

	public void setArenaStatus(CellNode[][] arenaStatus) {
		this.arenaStatus = arenaStatus;
	}

	public HashMap<Direction, Cell> getCellValues() {
		return cellValues;
	}

	public Cell getCellValue(Direction dir) {
		return this.cellValues.get(dir);
	}


}
