package models;

public enum Cell {
		EMPTY,
		FULL,
		FOOD,
		OBSTACLE
}
