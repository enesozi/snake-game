package models;

import java.awt.Color;
import java.awt.Point;

public class CellNode extends Node{

	private CellNode UP;
	private CellNode DOWN;
	private CellNode RIGHT;
	private CellNode LEFT;
	private Cell cell;
	private CellNode parent;
	private double g_score;
	private double f_score;
	private Color color;
	
	public CellNode(Point currentPoint) {
		super(currentPoint);
		this.cell = Cell.EMPTY;
		parent = UP = RIGHT = LEFT = DOWN = null;
		g_score = f_score = 0;
		color = Color.WHITE;
	}

	public CellNode getUP() {
		return UP;
	}

	public void setUP(CellNode uP) {
		UP = uP;
	}

	public CellNode getDOWN() {
		return DOWN;
	}

	public void setDOWN(CellNode dOWN) {
		DOWN = dOWN;
	}

	public CellNode getRIGHT() {
		return RIGHT;
	}

	public void setRIGHT(CellNode rIGHT) {
		RIGHT = rIGHT;
	}

	public CellNode getLEFT() {
		return LEFT;
	}

	public void setLEFT(CellNode lEFT) {
		LEFT = lEFT;
	}

	public Cell getCell() {
		return cell;
	}

	public void setCell(Cell cell) {
		this.cell = cell;
	}

	public CellNode getParent() {
		return parent;
	}

	public void setParent(CellNode parent) {
		this.parent = parent;
	}

	public double getG_score() {
		return g_score;
	}

	public void setG_score(double g_score) {
		this.g_score = g_score;
	}

	public double getF_score() {
		return f_score;
	}

	public void setF_score(double f_score) {
		this.f_score = f_score;
	}


	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

}
