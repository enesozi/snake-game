package models;
import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;

import models.Action.CATEGORY;
import simulation.Drawable;
import ui.GridPanel;

public class Snake implements Drawable {
	private final int MAX_LENGTH = 4;
	private final int REPRODUCE_LENGTH = 8;
	private ArrayList<Node> snakeBody;
	private Color bodyColor;
	private  int BFS_STEP;
	private ArrayList<Direction> bfsSteps;
	private  int DFS_STEP;
	private ArrayList<Direction> dfsSteps;
	private  int ASTAR_STEP;
	private ArrayList<Direction> astarSteps;
	public static int algo;
	public  Snake() {
		this.snakeBody = new ArrayList<Node>();
		this.bfsSteps = new ArrayList<Direction>();
		this.BFS_STEP = 0;
		this.dfsSteps = new ArrayList<Direction>();
		this.DFS_STEP = 0;
		this.astarSteps = new ArrayList<Direction>();
		this.ASTAR_STEP = 0;
		for(int i=MAX_LENGTH; i>0; i--) {
			this.snakeBody.add(new Node(new Point(i, 1)));
		}
		
		this.bodyColor = generateColor(25);
	}
	
	public  Snake(ArrayList<Node> snake) {
		this.snakeBody = snake;
		this.bfsSteps = new ArrayList<Direction>();
		this.BFS_STEP = 0;
		this.dfsSteps = new ArrayList<Direction>();
		this.DFS_STEP = 0;
		this.astarSteps = new ArrayList<Direction>();
		this.ASTAR_STEP = 0;
		this.bodyColor = generateColor(25);
	}
	
	private Color generateColor(int minPixel) {
		return new Color((int)Math.floor(minPixel+((200-minPixel)*Math.random())), 
				   (int)Math.floor(minPixel+((200-minPixel)*Math.random())), 
				   (int)Math.floor(minPixel+((200-minPixel)*Math.random())));
	}
	@Override
	public void draw(GridPanel panel) {
		panel.drawSquare(this.snakeBody.get(0).getXCoord(), this.snakeBody.get(0).getYCoord(), Color.RED);
		for(int i=1; i<this.snakeBody.size(); i++)
			panel.drawSquare(this.snakeBody.get(i).getXCoord(), this.snakeBody.get(i).getYCoord(), this.bodyColor);
	}
	
	public Action chooseAction(MazeInfo mazeInfo) {
		if (this.snakeBody.size() == REPRODUCE_LENGTH) {
			return new Action(CATEGORY.REPRODUCE);
		} else if (this.canEatFood(mazeInfo.getFood())) {
			return new Action(CATEGORY.EAT);
		} else {
			if(algo == 0) {
				Direction dir = getNextHeuristicMove(mazeInfo);
				if(dir != null)
					return new Action(CATEGORY.MOVE, dir);
			}else if(algo == 1) {
				if(BFS_STEP == 0) {
					calculateBFS(mazeInfo.getArenaStatus(),mazeInfo.getFood());
					if(bfsSteps.size() > 0) {
						//System.out.println(bfsSteps);
						Direction dir = bfsSteps.get(0);
						BFS_STEP += 1;
						return new Action(CATEGORY.MOVE, dir);
					}else {
						Direction dir = getNextHeuristicMove(mazeInfo);
						if(dir != null)
							return new Action(CATEGORY.MOVE, dir);
					}
				} else {
					Direction dir = null;
					if(BFS_STEP < bfsSteps.size()) {
						dir = bfsSteps.get(BFS_STEP);
						BFS_STEP += 1;
					}
					if (dir != null)
						return new Action(CATEGORY.MOVE, dir);
				}
			}else if(algo == 2) {
				if(DFS_STEP == 0) {
					calculateDFS(mazeInfo.getArenaStatus(),mazeInfo.getFood());
					if(dfsSteps.size() > 0) {
						//System.out.println(bfsSteps);
						Direction dir = dfsSteps.get(0);
						DFS_STEP += 1;
						return new Action(CATEGORY.MOVE, dir);
					}else {
						Direction dir = getNextHeuristicMove(mazeInfo);
						if(dir != null)
							return new Action(CATEGORY.MOVE, dir);
					}
				} else {
					Direction dir = null;
					if(DFS_STEP < dfsSteps.size()) {
						dir = dfsSteps.get(DFS_STEP);
						DFS_STEP += 1;
					}
					if (dir != null)
						return new Action(CATEGORY.MOVE, dir);
				}
			}else if(algo == 3) {
				if(ASTAR_STEP == 0) {
					calculateAStar(mazeInfo.getArenaStatus(),mazeInfo.getFood());
					if(astarSteps.size() > 0) {
						Direction dir = astarSteps.get(0);
						ASTAR_STEP += 1;
						return new Action(CATEGORY.MOVE, dir);
					}
				} else {
					Direction dir = null;
					if(ASTAR_STEP < astarSteps.size()) {
						dir = astarSteps.get(ASTAR_STEP);
						ASTAR_STEP += 1;
					}
					if (dir != null)
						return new Action(CATEGORY.MOVE, dir);
				}
			}
		}
		return null;
	}
	
	private void calculateAStar(CellNode[][] arenaStatus, Food food) {
		Node head = this.snakeBody.get(0);
		CellNode start = arenaStatus[head.getXCoord()][head.getYCoord()];
		CellNode end = arenaStatus[food.getXCoord()][food.getYCoord()];
		PriorityQueue<CellNode> nodeList = new PriorityQueue<CellNode>(new Comparator<CellNode>() {

			@Override
			public int compare(CellNode o1, CellNode o2) {
				if(o1.getF_score() > o2.getF_score())
					return 1;
				else if(o1.getF_score() < o2.getF_score())
					return -1;
				else return 0;
			}
		});
		start.setG_score(0);
		start.setF_score(calculateManhattanScore(start, end));
		nodeList.add(start);
		//HashSet<Point> visited = new HashSet<>();
		HashSet<Point> closed = new HashSet<>();
		//visited.add(start.getLocation());
		
		while(!nodeList.isEmpty()) {
			CellNode node = nodeList.poll();
			if(node.getLocation().equals(end.getLocation()))
				break;
			CellNode up = node.getUP();
			CellNode down = node.getDOWN();
			CellNode right = node.getRIGHT();
			CellNode left = node.getLEFT();
			CellNode[] neighbours = {up,down,right,left};
			closed.add(node.getLocation());
			for (CellNode neighbour : neighbours) {
				if(neighbour != null && neighbour.getCell() != Cell.FULL) {					
	                double temp_g_scores = node.getG_score() + calculateManhattanScore(node, neighbour);
	                		
	                double temp_f_score = temp_g_scores + calculateManhattanScore(neighbour,end);
	                
					if(closed.contains(neighbour.getLocation()) && temp_f_score > neighbour.getF_score())
						continue;
					//boolean hasBeenVisited = visited.contains(neighbour.getLocation());
					boolean hasBeenVisited = false;
					for (CellNode n : nodeList) {
						if(n.getLocation() == neighbour.getLocation()) {
							hasBeenVisited = true;
							break;
						}
					}
					if(temp_f_score < neighbour.getF_score() || !hasBeenVisited ) {
						if(hasBeenVisited) {
							for (CellNode n : nodeList) {
								if(n.getLocation() == neighbour.getLocation()) {
									nodeList.remove(neighbour);
									break;
								}
							}
						}
							
	                	neighbour.setParent(node);
	                	neighbour.setG_score(temp_g_scores);
						neighbour.setF_score(temp_f_score);
						nodeList.add(neighbour);
						//visited.add(neighbour.getLocation());						
					}				 
				}
			}
		}
		
		CellNode node = arenaStatus[food.getXCoord()][food.getYCoord()];
		while (node.getParent() != null) {
			CellNode parent = node.getParent();
			if(parent.getXCoord() < node.getXCoord())
				astarSteps.add(0,Direction.RIGHT);
			else if(parent.getXCoord() > node.getXCoord())
				astarSteps.add(0,Direction.LEFT);
			else if(parent.getYCoord() < node.getYCoord())
				astarSteps.add(0,Direction.DOWN);
			else if(parent.getYCoord() > node.getYCoord())
				astarSteps.add(0,Direction.UP);
			node = parent;
		}
		for(int x=0;x<arenaStatus.length;x++) 
			for(int y=0;y<arenaStatus[0].length;y++) {
				arenaStatus[x][y].setParent(null);
				arenaStatus[x][y].setF_score(0);
				arenaStatus[x][y].setG_score(0);
			}
	}

	private double calculateManhattanScore(CellNode start, CellNode end) {
		int unitCost = Math.abs(start.getXCoord()-end.getXCoord()) + Math.abs(start.getYCoord()-end.getYCoord());
		if(end.getColor() == Color.BLACK)
			return Double.POSITIVE_INFINITY*unitCost;
		else if(end.getColor() == Color.PINK)
			return 10000*unitCost;
		else if(end.getColor() == Color.GREEN)
			return 0.5*unitCost;
		return unitCost;
	}

	private void calculateBFS(CellNode[][] arenaStatus,Food food) {
		Node head = this.snakeBody.get(0);
		CellNode start = arenaStatus[head.getXCoord()][head.getYCoord()];
		CellNode end = arenaStatus[food.getXCoord()][food.getYCoord()];
		Queue<CellNode> nodeList = new LinkedList<CellNode>();
		nodeList.add(start);
		HashSet<Point> visited = new HashSet<>();
		visited.add(start.getLocation());
		while(!nodeList.isEmpty()) {
			CellNode node = nodeList.remove();
			if(node.getLocation().equals(end.getLocation()))
				break;
			CellNode up = node.getUP();
			CellNode down = node.getDOWN();
			CellNode right = node.getRIGHT();
			CellNode left = node.getLEFT();
			CellNode[] neighbours = {up,down,right,left};
			for (CellNode neighbour : neighbours) {
				if(neighbour != null && neighbour.getCell() != Cell.FULL) {
					if(!visited.contains(neighbour.getLocation())) {
						neighbour.setParent(node);
						visited.add(neighbour.getLocation());
						nodeList.add(neighbour);
					}
				}
			}
		}
		
		CellNode node = arenaStatus[food.getXCoord()][food.getYCoord()];
		while (node.getParent() != null) {
			CellNode parent = node.getParent();
			if(parent.getXCoord() < node.getXCoord())
				bfsSteps.add(0,Direction.RIGHT);
			else if(parent.getXCoord() > node.getXCoord())
				bfsSteps.add(0,Direction.LEFT);
			else if(parent.getYCoord() < node.getYCoord())
				bfsSteps.add(0,Direction.DOWN);
			else if(parent.getYCoord() > node.getYCoord())
				bfsSteps.add(0,Direction.UP);
			node = parent;
		}
		for(int x=0;x<arenaStatus.length;x++) 
			for(int y=0;y<arenaStatus[0].length;y++) 
				arenaStatus[x][y].setParent(null);
	}
	
	private void calculateDFS(CellNode[][] arenaStatus, Food food) {
		Node head = this.snakeBody.get(0);
		CellNode start = arenaStatus[head.getXCoord()][head.getYCoord()];
		CellNode end = arenaStatus[food.getXCoord()][food.getYCoord()];
		Stack<CellNode> nodeList = new Stack<CellNode>();
		nodeList.push(start);
		HashSet<Point> visited = new HashSet<>();
		visited.add(start.getLocation());
		while(!nodeList.isEmpty()) {
			CellNode node = nodeList.pop();
			if(node.getLocation().equals(end.getLocation()))
				break;
			CellNode up = node.getUP();
			CellNode down = node.getDOWN();
			CellNode right = node.getRIGHT();
			CellNode left = node.getLEFT();
			CellNode[] neighbours = {up,down,right,left};
			for (CellNode neighbour : neighbours) {
				if(neighbour != null && neighbour.getCell() != Cell.FULL) {
					if(!visited.contains(neighbour.getLocation())) {
						neighbour.setParent(node);
						visited.add(neighbour.getLocation());
						nodeList.add(neighbour);
					}
				}
			}
		}
		
		CellNode node = arenaStatus[food.getXCoord()][food.getYCoord()];
		while (node.getParent() != null) {
			CellNode parent = node.getParent();
			if(parent.getXCoord() < node.getXCoord())
				dfsSteps.add(0,Direction.RIGHT);
			else if(parent.getXCoord() > node.getXCoord())
				dfsSteps.add(0,Direction.LEFT);
			else if(parent.getYCoord() < node.getYCoord())
				dfsSteps.add(0,Direction.DOWN);
			else if(parent.getYCoord() > node.getYCoord())
				dfsSteps.add(0,Direction.UP);
			node = parent;
		}
		for(int x=0;x<arenaStatus.length;x++) 
			for(int y=0;y<arenaStatus[0].length;y++) 
				arenaStatus[x][y].setParent(null);
	}
	public boolean canEatFood(Food f) {
		Node head = this.snakeBody.get(0);
		return ((head.getXCoord() == f.getXCoord()) && Math.abs(head.getYCoord()-f.getYCoord()) == 1) ||
			   ((head.getYCoord() == f.getYCoord()) && Math.abs(head.getXCoord()-f.getXCoord()) == 1);
	}
	
	public void eatFood(Food f) {
		this.snakeBody.add(0,new Node(new Point(f.getXCoord(), f.getYCoord())));
	}
	
	public void clearBFS() {
		BFS_STEP = 0;
		bfsSteps.clear();
	}
	
	public void clearDFS() {
		DFS_STEP = 0;
		dfsSteps.clear();
	}
	public Snake reproduce() {
		ArrayList<Node> newSnake = new ArrayList<Node>();
		Node lastNode;
		for (int i = 1; i <= REPRODUCE_LENGTH-MAX_LENGTH; i++) {
			lastNode = this.snakeBody.get(this.snakeBody.size()- 1);
			newSnake.add(new Node(new Point(lastNode.getXCoord(), lastNode.getYCoord())));
			this.snakeBody.remove(this.snakeBody.size()-1);
		}
		return new Snake(newSnake);
	}

	 
	private Direction getNextHeuristicMove(MazeInfo mazeInfo) {
		Food food = mazeInfo.getFood();
		Node head = this.snakeBody.get(0);
		if (food.getXCoord() > head.getXCoord()) {
			if (mazeInfo.getCellValue(Direction.RIGHT) == Cell.EMPTY) {
				return Direction.RIGHT;
			}
		}
		if (food.getXCoord() < head.getXCoord()) {
			if (mazeInfo.getCellValue(Direction.LEFT) == Cell.EMPTY) {
				return Direction.LEFT;
			}
		}
		if (food.getYCoord() > head.getYCoord()) {
			if (mazeInfo.getCellValue(Direction.DOWN) == Cell.EMPTY) {
				return Direction.DOWN;
			}
		}
		if (food.getYCoord() < head.getYCoord()) {
			if (mazeInfo.getCellValue(Direction.UP) == Cell.EMPTY) {
				return Direction.UP;
			}
		}
		
		ArrayList<Direction> freeDirs = mazeInfo.getFreeDirections();

		if (!freeDirs.isEmpty())
			return freeDirs.get(new Random().nextInt(freeDirs.size()));
		return null;
	}

	public void move(Direction direction) {
		
		Node newHead = new Node(this.snakeBody.get(0).getLocation());
		if (direction == Direction.UP) {
			newHead.setYCoord(newHead.getYCoord()-1);
		} else if (direction == Direction.DOWN) {
			newHead.setYCoord(newHead.getYCoord()+1);
		} else if (direction == Direction.RIGHT) {
			newHead.setXCoord(newHead.getXCoord()+1);
		} else {
			newHead.setXCoord(newHead.getXCoord()-1);
		}
		this.snakeBody.add(0, newHead);
		this.snakeBody.remove(this.snakeBody.size()-1);
	}

	public ArrayList<Node> getSnakeBody() {
		return snakeBody;
	}


	public void setSnakeBody(ArrayList<Node> snakeBody) {
		this.snakeBody = snakeBody;
	}


	public Color getBodyColor() {
		return bodyColor;
	}


	public void setBodyColor(Color bodyColor) {
		this.bodyColor = bodyColor;
	}

	public void clearAStar() {
		ASTAR_STEP = 0;
		astarSteps.clear();
	}

}
