package models;

import java.awt.Color;
import java.awt.Point;

import simulation.Drawable;
import ui.GridPanel;

public class Food extends Point implements Drawable{
	private Point location;
	private Color foodColor;
	public Food(Point currentPoint) {
		this.location = new Point(currentPoint.x,currentPoint.y);	
		this.setFoodColor(Color.YELLOW);
	}
	
	public Point getLocation() {
		return this.location;
	}
	
	public int getXCoord() {
		return  this.location.x;
	}

	public int getYCoord() {
		return  this.location.y;
	}
	
	public void setXCoord(int x) {
		this.location.x = x;
	}

	public void setYCoord(int y) {
		this.location.y = y;
	}

	@Override
	public void draw(GridPanel panel) {
		panel.drawSquare(this.location.x, this.location.y, this.foodColor);
	}

	public Color getFoodColor() {
		return foodColor;
	}

	public void setFoodColor(Color foodColor) {
		this.foodColor = foodColor;
	}
}
