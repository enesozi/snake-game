package main;

import java.awt.EventQueue;

import arena.GameArena;
import models.Snake;
import ui.ApplicationWindow;

public class Main {

	public static void main(String[] args) {
		int gridWidth = new Integer(args[0]).intValue();
		int gridHeight = new Integer(args[1]).intValue();
		int gridSquareSize = new Integer(args[2]).intValue();
		int frameRate = new Integer(args[3]).intValue();
		int algo = new Integer(args[4]).intValue();
		Snake.algo = algo;
		EventQueue.invokeLater(() -> {
			GameArena game = new GameArena(gridWidth, gridHeight, gridSquareSize, frameRate);
			// Create application window that contains the game panel
			ApplicationWindow window = new ApplicationWindow(game.getGamePanel());
			window.getFrame().setVisible(true);
			
			game.start();
		});
	}

}
