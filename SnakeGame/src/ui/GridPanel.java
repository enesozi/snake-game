package ui;

import javax.swing.*;

import models.Snake;

import java.awt.*;
import java.awt.image.BufferedImage;


public class GridPanel extends JPanel {

    private final int gamePanelWidth;
    private final int gamePanelHeight;

    private BufferedImage gameImage;

    private int gridSquareSize;

    /**
     * Constructs a grid panel that can be drawn on
     * @param gridWidth width of the grid
     * @param gridHeight height of the grid
     * @param gridSquareSize size of a grid square in pixels
     */
    public GridPanel(int gridWidth, int gridHeight, int gridSquareSize) {
        this.gridSquareSize = gridSquareSize;
        gamePanelWidth = gridWidth * gridSquareSize;
        gamePanelHeight = gridHeight * gridSquareSize;
        gameImage = new BufferedImage(gamePanelWidth, gamePanelHeight, BufferedImage.TYPE_INT_ARGB);
        setBackground(Color.WHITE);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(gamePanelWidth, gamePanelHeight);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if (gameImage != null) {
            g.drawImage(gameImage, 0, 0, null);
        }
    }

    /**
     * Repaints the panel to white
     */
    public void clearCanvas() {
        Graphics g = gameImage.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, gameImage.getWidth(), getHeight());
        if(Snake.algo == 3) {
        	int x1 = getGridWidth()/2 - 2;
        	int x2 = getGridWidth()/2 + 2;
        	int y1 = getGridHeight()/2 - 2 ;
        	int y2 = getGridHeight()/2 + 2 ;
        	for(int x=x1;x<=x2;x++)
        		for(int y=y1;y<=y2;y++) {
        			drawSquare(x, y, Color.BLACK);
        			drawString("inf", x, y);
        		}
        	x1 = getGridWidth()/2 - 10;
        	x2 = getGridWidth()/2 - 6 ;
        	y1 = getGridHeight()/2 - 5 ;
        	y2 = getGridHeight()/2 - 1 ;
        	for(int x=x1;x<=x2;x++)
        		for(int y=y1;y<=y2;y++) {
        			drawSquare(x, y, Color.GREEN);
        			drawString("0", x, y);
        		}
        	x1 = getGridWidth()/2 + 6;
        	x2 = getGridWidth()/2 + 10 ;
        	y1 = getGridHeight()/2 - 8 ;
        	y2 = getGridHeight()/2 - 4 ;
        	for(int x=x1;x<=x2;x++)
        		for(int y=y1;y<=y2;y++) {
        			drawSquare(x, y, Color.PINK);
        			drawString("1k", x, y);
        		}
        			
        }
        g.dispose();
    }

    /**
     * Returns the number of grid squares along the width
     * @return grid width
     */
    public int getGridWidth() {
        return gamePanelWidth / gridSquareSize;
    }

    /**
     * Returns the number of grid squares along the height
     * @return grid height
     */
    public int getGridHeight() {
        return gamePanelHeight / gridSquareSize;
    }

    /**
     * Draws gridlines
     */
    public void drawGrid() {
        Graphics g = gameImage.getGraphics();
        g.setColor(Color.LIGHT_GRAY);

        // vertical grid
        for (int i = 0; i < gamePanelWidth / gridSquareSize; i++) {
            int lineX = i * gridSquareSize;
            g.drawLine(lineX, 0, lineX, gamePanelHeight);
        }
        // horizontal grid
        for (int i = 0; i < gamePanelHeight / gridSquareSize; i++) {
            int lineY = i * (gridSquareSize);
            g.drawLine(0, lineY, gamePanelWidth, lineY);
        }
        g.dispose();
    }

    /**
     * Draws a filled square in the given grid position
     * @param gridX x position
     * @param gridY y position
     * @param color fill color of the square
     */
    public void drawSquare(int gridX, int gridY, Color color) {
        if (gridX < 0 || gridY < 0 ||
                gridX >= gamePanelWidth / gridSquareSize || gridY >= gamePanelHeight / gridSquareSize) {
            return;
        }
        Graphics g = gameImage.getGraphics();
        g.setColor(color);
        int x = gridX * gridSquareSize + 1;
        int y = gridY * gridSquareSize + 1;
        g.fillRect(x, y, gridSquareSize - 1, gridSquareSize - 1);
        g.dispose();
    }
    
    /**
     * Draws a string in the given grid position
     * @param gridX x position
     * @param gridY y position
     */
    public void drawString(String str, int gridX, int gridY) {
        if (gridX < 0 || gridY < 0 ||
                gridX >= gamePanelWidth / gridSquareSize || gridY >= gamePanelHeight / gridSquareSize) {
            return;
        }
        Graphics g = gameImage.getGraphics();
        int x = gridX * gridSquareSize + 1;
        int y = gridY * gridSquareSize + 1;
        g.drawString(str, x, y);
        g.dispose();
    }

    /**
     * Draws a small filled square in the given grid position
     * @param gridX x position
     * @param gridY y position
     * @param color fill color of the square
     */
    public void drawSmallSquare(int gridX, int gridY, Color color) {
        if (gridX < 0 || gridY < 0 ||
                gridX >= gamePanelWidth / gridSquareSize || gridY >= gamePanelHeight / gridSquareSize) {
            return;
        }
        Graphics g = gameImage.getGraphics();
        g.setColor(color);
        int x = gridX * gridSquareSize + 3;
        int y = gridY * gridSquareSize + 3;
        g.fillRect(x, y, gridSquareSize - 5, gridSquareSize - 5);
        g.dispose();
    }

}
